class Pet:
    def makeSound(self):
        print("Silence")

class Dog(Pet):
    def makeSound(self):
        print("Bork")

class Cat(Pet):
    def makeSound(self):
        print("Alex")

pet = Pet()
dog = Dog()
cat = Cat()

pet.makeSound()
dog.makeSound()
cat.makeSound()